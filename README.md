RushMyNews
==========


Notes à l'attention de l'équipe de développement.


Utilisation courante de git
---------------------------

Le cas d'utilisation standard est :
1. récupérer le dépôt,
2. récupérer la branche sur laquelle travailler `git checkout --track -b branche origin/branche`,
3. s'assigner une tâche dans le [framagit du projet](https://framagit.org/rmn/fidrid/issues),
4. effectuer les travaux - en respectant les [coding standards symfony](http://symfony.com/doc/current/contributing/code/standards.html),
5. committer le travail en respectant le [formattage du message de commit](#commitmsg),
6. alerter les autres sur le salon de discussion et/ou mail.



<a name="commitmsg"></a>Formattage du message de commit
-------------------------------------------------------

* il commence par `issue #` suivi du numéro de tâche, puis `:` et un espace  (ex. `issue #14: `),
* il est suivi d'un descriptif court du travail effectué,
* la première ligne doit faire moins de 50 caractères,
* la deuxième ligne est vide,
* ensuite, donner un descriptif des difficultés ou particularités de la tâche.

Gardez à l'esprit que ce message sera lu par un technicien.

Exemple complet:
```
#239: Correctif cosmétique sur le titre des cats

Modifications simple en deux endroits du code. J'ai oublié le fichier
de configuration général dans un premier temps...
```

Navigation entre branches
-------------------------

* Sélectionner une branche : `git checkout branche`
* Créer une branche locale : `git branch branche`
* Pour supprimer une branche locale : `git branch -d branche`

Paramétrage de votre nom d'utilisateur
--------------------------------------

Sur toutes les machines où vous travaillez sur le projet, assurez-vous d'utiliser le même nom d'utilisateur partout. Pour cela, lors du clone du projet, il faut exécuter les commandes :
* `git config --global user.name "John Doe"`
* `git config --global user.email johndoe@example.com`
