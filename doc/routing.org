
  |   | Page                          | Chemin                            | Controller | Method |
  |---+-------------------------------+-----------------------------------+------------+--------|
  | 1 | Homepage déconnectée          | ~/~                               | Tuto       | GET    |
  | 5 | Intercalaire tutoriel         | ~/tuto/index~                     | Tuto       | GET    |
  |   | Tutoriel itself               | ~/tuto/presentation~              | Tuto       | GET    |
  |---+-------------------------------+-----------------------------------+------------+--------|
  | 2 | Formulaire de connexion       | ~/login~                          | User       | GET    |
  | 3 | Formulaire création de compte | ~/register~                       | User       | GET    |
  |---+-------------------------------+-----------------------------------+------------+--------|
  | 4 | Homepage Connectée            | ~/feed/list~                      | Feed       | GET    |
  | 6 | Recherche de Flux             | ~/feed/search~                    | Feed       | GET    |
  |   | Ajout de Flux                 | ~/feed/add~                       | Feed       | POST   |
  |---+-------------------------------+-----------------------------------+------------+--------|
  | 7 | Formulaire création catégorie | ~/category/new~                   | Category   | GET    |
  |   | Ajout de Catégorie            | ~/category/new~                   | Category   | POST   |
  |   | Affichage de Catégorie (N)    | ~/category/{cat}/~                | Category   | GET    |
  |   | Suppression de Catégorie      | ~/category/{cat}/delete~          | Category   | POST   |
  |---+-------------------------------+-----------------------------------+------------+--------|
  | 8 | Edition des préférences       | ~/{user}/edit~                    | User       | GET    |
  |   | Suppression d'un flux         | ~/{user}/edit/feed/{feed}/delete~ | User       | POST   |

