rushmynews
==========


Déploiement de l'environnement de développement
-----------------------------------------------

### Prérequis

| Commande    | Installation Debian (Ubuntu, Mint, etc,) |
|-------------|------------------------------------------|
| php         | `sudo apt install php5-cli`              |
| composer    | https://getcomposer.org/download/        |
| node        | `sudo apt install nodejs`                |
| npm         | `sudo apt install npm`                   |
| gulp        | `sudo npm install -g gulp-cli`           |

**Attention**, les Debian-like anciennes versions installent un exécutable nommé ```nodejs``` et pas ```node``` or cela pose problème avec tous les modules npm.

Le seul moyen simple de contourner le pb que les internet ont trouvé est de lancer la commande :
```
ln -s /usr/bin/nodejs /usr/bin/node
```
Commande à exécuter _après_ l'installation du package, évidemment.

| Modules Php | Installation Debian                      |
|-------------|------------------------------------------|
| php-intl    | `sudo apt install php-intl`              |
| php-curl    | `sudo apt install php-curl`              |


### Checklist pour l'exécution du backend php

* installer les vendors : *à la racine du projet symfony*, lancer la commande `composer install` - laisser les valeurs par défaut pour toutes les questions
* déployer les _assets_ : `php bin/console assets:install --relative --symlink`


### Checklist compilation des assets

Attention, cette section n'est à suivre que si vous souhaitez modifier ces assets.

Les assets regroupent les fichiers de développement _front-end_ ( CSS, javascript, _spritesheet_ et polices ).


#### Installation des dépendances

`npm` joue le rôle de `composer` pour les packages javascript. Les dépendances sont indiquées dans le fichier `package.json`.

Pour les installer, placez-vous à la racine du projet symfony puis exécutez la commande :
```
npm install
```

#### Génération de la spritesheet SVG

Pour générer la spritesheet, placez-vous à la racine du projet symfony et exécutez la commande :
```
gulp svgstore
```

Le fichier `/src/RMNBundle/Resources/public/img/sprites.svg` est alors re-généré.

### Préparation de la base de données pour développements et tests

Pour pouvoir utiliser la base contenant les données de test, il faut jouer les commandes ci-dessous - *Attention, cela videra totalement votre base de données !*

A la racine du projet Symfony :
```
php bin/console doctrine:database:drop --force
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
php bin/console doctrine:fixtures:load --no-interaction
```

### Mise à jour de la table articles

Pour mettre à jour le contenu de la table articles à partir de la liste des flux enregistrés, il faut exécuter la commande :
```
php bin/console rush:articles:update
```

Pour purger cette table, exécuter la commande :
```
php bin/console rush:articles:purge
```
Le paramétrage des jours de rétention a lieu dans le fichier d'entité de la classe Article.


Autres commandes utiles
-----------------------

Pour visualiser les journaux (logs) au fil de l'eau, placez-vous à la racine du projet symfony et exécutez la commande :
```
tail -f var/logs/*.log
```
