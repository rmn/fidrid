var gulp     = require('gulp');
var svgstore = require('gulp-svgstore');
var svgmin   = require('gulp-svgmin');
var path     = require('path');

var svg_selection_dir = 'src/RMNBundle/Resources/public/img/sprites';
var svg_sprite_dir    = 'src/RMNBundle/Resources/public/img';

gulp.task('default', function() {
  // place code for your default task here
});

gulp.task('svgstore', function () {

    return gulp
        .src(svg_selection_dir+'/*.svg')
        .pipe(svgmin(function (file) {
            var prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix + '-',
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore())
        .pipe(gulp.dest(svg_sprite_dir));

});
