<?php

namespace RMNBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RushArticlesPurgeCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rush:articles:purge')
            ->setDescription('Purge articles list from Database')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('argument');
        $updater  = $this->getContainer()->get('rmn.articles.purge');

        // Argument magique :
        if ( $argument === 'all' )
        {
            $output->writeln($updater->runPurgeAll());
            exit(0);
        }

        // Cas général
        $output->writeln($updater->runPurge());
        exit(0);
    }

}
