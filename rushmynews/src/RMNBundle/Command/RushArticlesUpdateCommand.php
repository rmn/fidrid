<?php

namespace RMNBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RushArticlesUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rush:articles:update')
            ->setDescription('Fetch and store articles from known RSS feeds')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $updater = $this->getContainer()->get('rmn.articles.update');
        $infos   = $updater->runUpdate();

        $output->writeln($infos);
    }

}
