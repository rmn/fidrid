<?php

namespace RMNBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RushFeedUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rush:feed:update')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('argument');

        $updater = $this->getContainer()->get('rmn.feed.update');
        $infos   = $updater->runUpdate($argument);

        $output->writeln($infos['url']."\n"
                         .$infos['title']."\n"
                         .$infos['icon']);
    }

}
