<?php

namespace RMNBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use RMNBundle\Entity\Fluxrss;
use RMNBundle\Entity\Article;
use RMNBundle\Entity\User;
use RMNBundle\Entity\Mesflux;
use RMNBundle\Form\FluxrssType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FeedController extends Controller
{
    public function listAction()
    {
        $user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();

        $mesFluxRep    = $entityManager->getRepository('RMNBundle:Mesflux');
        $articlesRep   = $entityManager->getRepository('RMNBundle:Article');

        $fluxList     = $mesFluxRep->findBy(array('idUserMesflux' => $user));
        $articlesList = $articlesRep->findBy(
            array('idFluxrss_article' => $fluxList),
            array('date_article'      => 'DESC')
        );

        return $this->render('RMNBundle:Feed:list.html.twig', array(
            'user'     => $user,
            'articles' => $articlesList,
        ));
    }

    public function searchAction()
    {
      $feed = new Fluxrss();

      $formBuilder = $this->createFormBuilder($feed)
        ->setAction($this->generateUrl('feed_add'))
        ->setMethod('POST')
        ->add('urlFluxrss', TextType::class)
        ->add('Ajouter', SubmitType::class)
        ->getForm();


      return $this->render('RMNBundle:Feed:search.html.twig', array(
          'user' => $this->getUser(),
          'ajouter_flux'=> $formBuilder->createView(),
      ));
    }

    public function addAction(Request $request)
    {
      $feed = new Fluxrss();

      $formBuilder = $this->createFormBuilder($feed)
        ->setAction($this->generateUrl('feed_add'))
        ->setMethod('POST')
        ->add('urlFluxrss', TextType::class)
        ->add('Ajouter', SubmitType::class)
        ->getForm();

      if ($request->isMethod('POST')) {

          var_dump($request);
          
          $formBuilder->handleRequest($request);

        $flux = $formBuilder->get('urlFluxrss')->getData();

         // On récupère le repository de l'entité
         $em = $this->getDoctrine()->getManager();
         $repository = $em->getRepository('RMNBundle:Fluxrss');
         // On récupère l'entité correspon

         $verificationUrl = $repository->findByUrlFluxrss($flux);
        //  $verificationUrl = $entityUrl->getUrlFluxrss();

        // Si le flux n'existe pas edans la base FluxRSS, on enregistre puis on l'ajoute pour l'utilisateur
         if (!$verificationUrl) {
           // insertion dans la base fluxRSS

           $updater = $this->container->get('rmn.feed.update');

           $infos   = $updater->runUpdate($flux);
           $titre = $infos['title'];
           $icone = $infos['icon'];


           $feed ->setNomFluxrss($titre);
           $feed ->setUrlFluxrss($flux);
           $feed ->setIconeFLuxrss($icone);


           // Insertion dans la table Mesflux pour l'Utilisateur.

            if ($formBuilder->isValid()) {

              $em = $this->getDoctrine()->getManager();
              $em->persist($feed);
              $em->flush();

              $request->getSession()->getFlashBag()->add('Success', 'Feed saved.');

              return $this->redirectToRoute('ajouter_flux');
            }
          }else {
            return $this->redirectToRoute('mockup_home');


        }

      }

      return $this->render('RMNBundle:Feed:add.html.twig', array(
          'user' => $this->getUser(),
        'ajouter_flux'=> $formBuilder->createView(),
      ));
    }

}
