<?php

namespace RMNBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use RMNBundle\Entity\Fluxrss;
use RMNBundle\Form\FluxrssType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class MockupController extends Controller
{

    private $user = array(
        'name' => "bogoss44",
    );

    private $tags = array(
        "actus",
        "logiciel",
        "travail",
    );

    private $news = array(
        array(
            "feed"         => "Le Monde",
            "feedurl"      => "#",
            "title"        => "Rainbow Dash met un terme à sa carrière",
            "titleurl"     => "#",
            "published_at" => "2016-08-22 15:32:22",
            "favicon"      => "lemonde.ico",
            "content"      => "<p>Lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit, scelerisque in dictum non, consectetur a erat! Ultrices eros, in cursus turpis?</p>",
            "tags"         => array('politique'),
        ),
        array(
            "feed"         => "L'équipe",
            "feedurl"      => "#",
            "title"        => "AppleJack quitte le Bayern Munich pour les Gunners",
            "titleurl"     => "#",
            "published_at" => "2016-08-23 09:01:22",
            "favicon"      => "lequipe.ico",
            "content"      => "<p>Lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit, scelerisque in dictum non, consectetur a erat! Ultrices eros, in cursus turpis?</p>",
            "tags"         => array('politique','travail'),
        ),
        array(
            "feed"         => "Voici",
            "feedurl"      => "#",
            "title"        => "Pinkie Pie fait son coming out",
            "titleurl"     => "#",
            "published_at" => "2016-08-24 12:32:22",
            "favicon"      => "voici.ico",
            "content"      => "<p>Lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit, scelerisque in dictum non, consectetur a erat! Ultrices eros, in cursus turpis?</p>",
            "tags"         => array('politique','travail'),
        ),
        array(
            "feed"         => "Le Monde",
            "feedurl"      => "#",
            "title"        => "Le projet de loi Celestia soulève la colère de l'opposition",
            "titleurl"     => "#",
            "published_at" => "2016-08-25 22:15:22",
            "favicon"      => "lemonde.ico",
            "content"      => "<p>Lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit, scelerisque in dictum non, consectetur a erat! Ultrices eros, in cursus turpis?</p>",
            "tags"         => array('politique'),
        ),
        array(
            "feed"         => "Linuxfr.org",
            "feedurl"      => "#",
            "title"        => "Vidéo exclusive : Saint Ignucius se cure les pieds",
            "titleurl"     => "#",
            "published_at" => "2016-08-26 10:54:22",
            "favicon"      => "linuxfr.ico",
            "content"      => "<p>Lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit, scelerisque in dictum non, consectetur a erat! Ultrices eros, in cursus turpis?</p>",
            "tags"         => array('info'),
        ),
        array(
            "feed"         => "FreeBSD",
            "feedurl"      => "#",
            "title"        => "btrfs WTF ? ZFS is the one and only",
            "titleurl"     => "#",
            "published_at" => "2016-08-27 15:23:22",
            "favicon"      => "freebsd.ico",
            "content"      => "<p>Lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit, scelerisque in dictum non, consectetur a erat! Ultrices eros, in cursus turpis?</p>",
            "tags"         => array('info'),
        ),
    );

    public function homeAction(Request $request)
    {
        $feed = new Fluxrss();

        $formBuilder = $this->createFormBuilder($feed)
          ->setAction($this->generateUrl('ajouter_flux'))
          ->setMethod('POST')
          ->add('urlFluxrss', TextType::class)
          ->add('Ajouter', SubmitType::class)
          ->getForm();

        return $this->render('RMNBundle:Mockup:home.html.twig', array(
            'user'   => $this->user,
            'tags'   => $this->tags,
            'news'   => $this->news,
            'ajouter_flux'=> $formBuilder->createView(),
        ));
    }

    public function addTagAction()
    {
        $feed = new Fluxrss();

        $formBuilder = $this->createFormBuilder($feed)
          ->setAction($this->generateUrl('ajouter_flux'))
          ->setMethod('POST')
          ->add('urlFluxrss', TextType::class)
          ->add('Ajouter', SubmitType::class)
          ->getForm();

        return $this->render('RMNBundle:Mockup:empty.html.twig', array(
            'user' => $this->user,
            'ajouter_flux'=> $formBuilder->createView(),
        ));
    }

    public function addFeedAction(Request $request)
    {
        $feed = new Fluxrss();

        $formBuilder = $this->createFormBuilder($feed)
          ->setAction($this->generateUrl('ajouter_flux'))
          ->setMethod('POST')
          ->add('urlFluxrss', TextType::class)
          ->add('Ajouter', SubmitType::class)
          ->getForm();


        if ($request->isMethod('POST')) {

          $formBuilder->handleRequest($request);

          if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($feed);
            $em->flush();

            $request->getSession()->getFlashBag()->add('Success', 'Feed saved.');

            return $this->$redirectToRoute('RMNBundle:Mockup:home.html.twig');
          }
        }


        return $this->render('RMNBundle:Mockup:addFeed.html.twig', array(
          'user' => $this->user,
          'ajouter_flux'=> $formBuilder->createView(),
        ));
    }

    public function searchFeedAction()
  {
      $feed = new Fluxrss();

      $formBuilder = $this->createFormBuilder($feed)
        ->setAction($this->generateUrl('ajouter_flux'))
        ->setMethod('POST')
        ->add('urlFluxrss', TextType::class)
        ->add('Ajouter', SubmitType::class)
        ->getForm();


      return $this->render('RMNBundle:Mockup:searchFeed.html.twig', array(
          'user' => $this->user,
          'ajouter_flux'=> $formBuilder->createView(),
      ));
  }
}
