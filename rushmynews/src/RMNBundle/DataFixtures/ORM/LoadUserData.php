<?php

namespace RMNBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use RMNBundle\Entity\User;
use RMNBundle\Entity\Fluxrss;
use RMNBundle\Entity\Mesflux;

use DateTime;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{

    // Merci: goo.gl/od5s4k
    //    et  goo.gl/o14tDm !!
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // Utilise un user_manager, notamment pour paramétrer certains
        // champs en base de données, qui sont impossibles à remplir
        // via Doctrine - pas de méthode setSalt().
        $userManager = $this->container->get('fos_user.user_manager');

        // Creation d'un utilisateur administrateur
        //
        $admin = $userManager->createUser();
        $admin->setUsername('admin');
        $admin->setEmail('admin@example.com');
        $admin->setPlainPassword('test');
        $admin->setEnabled(true);
        $admin->setRoles(array('ROLE_ADMIN'));

        // Creation d'un utilisateur lambda
        //
        $user1 = $userManager->createUser();
        $user1->setUsername('user1');
        $user1->setEmail('user1@example.com');
        $user1->setPlainPassword('test');
        $user1->setEnabled(true);
        $user1->setRoles(array('ROLE_USER'));

        // Creation d'un autre utilisateur lambda
        //
        $user2 = $userManager->createUser();
        $user2->setUsername('user2');
        $user2->setEmail('user2@example.com');
        $user2->setPlainPassword('test');
        $user2->setEnabled(true);
        $user2->setRoles(array('ROLE_USER'));

        // "Persiste" les données
        //
        $userManager->updateUser($admin, true);
        $userManager->updateUser($user1, true);
        $userManager->updateUser($user2, true);

        // Ajout de flux RSS
        //
        $feed_1 = new Fluxrss();
        $feed_2 = new Fluxrss();
        $feed_3 = new Fluxrss();

        $feed_1->setNomFluxrss('Linuxfr - Actus');
        $feed_1->setUrlFluxrss('http://linuxfr.org/news.atom');

        $feed_2->setNomFluxrss('Le Monde - La Une');
        $feed_2->setUrlFluxrss('http://www.lemonde.fr/rss/une.xml');

        $feed_3->setNomFluxrss('L\'Équipe - Général');
        $feed_3->setUrlFluxrss('http://www.lequipe.fr/rss/actu_rss.xml');

        $manager->persist($feed_1);
        $manager->persist($feed_2);
        $manager->persist($feed_3);
        $manager->flush();

        // Liaison des flux aux utilisateurs
        //
        $monFlux_1 = new Mesflux();
        $monFlux_2 = new Mesflux();
        $monFlux_3 = new Mesflux();

        $now = new DateTime('now');

        $monFlux_1->setIdUserMesFlux($user1);
        $monFlux_1->setIdFluxrssMesFlux($feed_2);
        $monFlux_1->setDateAjoutMesflux($now);

        $monFlux_2->setIdUserMesFlux($user1);
        $monFlux_2->setIdFluxrssMesFlux($feed_3);
        $monFlux_2->setDateAjoutMesflux($now);

        $monFlux_3->setIdUserMesFlux($user2);
        $monFlux_3->setIdFluxrssMesFlux($feed_1);
        $monFlux_3->setDateAjoutMesflux($now);

        $manager->persist($monFlux_1);
        $manager->persist($monFlux_2);
        $manager->persist($monFlux_3);
        $manager->flush();
    }
}