<?php

namespace RMNBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Article.
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="RMNBundle\Repository\ArticleRepository")
 */
class Article
{

    const MAX_AGE = 10;         // Article retention duration (days)

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre_article", type="string", length=100)
     */
    private $titre_article;

    /**
     * @var datetime
     *
     * @ORM\Column(name="date_article", type="datetime", length=100)
     */
    private $date_article;

    /**
     * @var string
     *
     * @ORM\Column(name="url_article", type="string", length=250)
     */
    private $url_article;


    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="RMNBundle\Entity\Fluxrss")
     * @ORM\JoinColumn(nullable=false))
     */
    private $idFluxrss_article;

    /**
     * @var text
     *
     * @ORM\Column(name="content_article", type="text", nullable=true)
     */
    private $content_article;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre.
     *
     * @param string $titre
     *
     * @return Article
     */
    public function setTitre($titre)
    {
        $this->titre_article = $titre;

        return $this;
    }

    /**
     * Get titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre_article;
    }

    /**
     * Set date_article.
     *
     * @param datetime $date
     *
     * @return Article
     */
    public function setDate($date)
    {
        $this->date_article = $date;

        return $this;
    }

    /**
     * Get date_article.
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date_article;
    }

    /**
     * Set url_article.
     *
     * @param string $url_article
     *
     * @return Article
     */
    public function setUrlArticle($url_article)
    {
        $this->url_article = $url_article;

        return $this;
    }

    /**
     * Get url_article.
     *
     * @return string
     */
    public function getUrlArticle()
    {
        return $this->url_article;
    }

    /**
     * Set idFluxrss_article.
     *
     * @param string $id
     *
     * @return Article
     */
    public function setIdFluxrss($id)
    {
        $this->idFluxrss_article = $id;

        return $this;
    }

    /**
     * Get idFluxrss_article.
     *
     * @return string
     */
    public function getIdFluxrss()
    {
        return $this->idFluxrss_article;
    }

    /**
     * Set content_article.
     *
     * @param string $content_article
     *
     * @return Article
     */
    public function setContentArticle($content_article)
    {
        $this->content_article = $content_article;

        return $this;
    }

    /**
     * Get content_article.
     *
     * @return string
     */
    public function getContentArticle()
    {
        return $this->content_article;
    }

    /**
     * Set idUserArticle
     *
     * @param integer $idUserArticle
     *
     * @return Article
     */
    public function setIdUserArticle($idUserArticle)
    {
        $this->idUserArticle = $idUserArticle;

        return $this;
    }

    /**
     * Get idUserArticle
     *
     * @return int
     */
    public function getIdUserArticle()
    {
        return $this->idUserArticle;
    }
}
