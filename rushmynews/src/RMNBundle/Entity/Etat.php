<?php

namespace RMNBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Etat
 *
 * @ORM\Table(name="etat")
 * @ORM\Entity(repositoryClass="RMNBundle\Repository\EtatRepository")
 */
class Etat
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="RMNBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false))
     */
    private $idUserEtat;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="RMNBundle\Entity\Article")
     * @ORM\JoinColumn(nullable=false))
     */
    private $idArticleEtat;

    /**
     * @var string
     *
     * @ORM\Column(name="lu", type="string", length=3)
     */
    private $lu;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUserMesflux
     *
     * @param integer $idUserEtat
     *
     * @return Etat
     */
    public function setIdUserEtat($idUserEtat)
    {
        $this->idUserEtat = $idUserEtat;

        return $this;
    }

    /**
     * Get idUserEtat
     *
     * @return int
     */
    public function getIdUserEtat()
    {
        return $this->idUserEtat;
    }


    /**
     * Set idArticleEtat
     *
     * @param integer $idArticleEtat
     *
     * @return Etat
     */
    public function setIdArticleEtat($idArticleEtat)
    {
        $this->idArticleEtat = $idArticleEtat;

        return $this;
    }

    /**
     * Get idArticleEtat
     *
     * @return int
     */
    public function getIdArticleEtat()
    {
        return $this->idArticleEtat;
    }



    /**
     * Set lu
     *
     * @param string $lu
     *
     * @return Etat
     */
    public function setLu($lu)
    {
        $this->lu = $lu;

        return $this;
    }

    /**
     * Get lu
     *
     * @return string
     */
    public function getLu()
    {
        return $this->lu;
    }
}
