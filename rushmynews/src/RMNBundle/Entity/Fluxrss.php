<?php

namespace RMNBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fluxrss
 *
 * @ORM\Table(name="fluxrss")
 * @ORM\Entity(repositoryClass="RMNBundle\Repository\FluxrssRepository")
 */
class Fluxrss
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_fluxrss", type="string", length=50, unique=true)
     */
    private $nomFluxrss;

    /**
     * @var string
     *
     * @ORM\Column(name="url_fluxrss", type="string", length=255, unique=true)
     */
    private $urlFluxrss;

    /**
     * @var string
     *
     * @ORM\Column(name="icon_fluxrss", type="string", length=255, nullable=true)
     */
    private $iconeFluxrss;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomFluxrss
     *
     * @param string $nomFluxrss
     *
     * @return Fluxrss
     */
    public function setNomFluxrss($nomFluxrss)
    {
        $this->nomFluxrss = $nomFluxrss;

        return $this;
    }

    /**
     * Get nomFluxrss
     *
     * @return string
     */
    public function getNomFluxrss()
    {
        return $this->nomFluxrss;
    }

    /**
     * Set urlFluxrss
     *
     * @param string $urlFluxrss
     *
     * @return Fluxrss
     */
    public function setUrlFluxrss($urlFluxrss)
    {
        $this->urlFluxrss = $urlFluxrss;

        return $this;
    }

    /**
     * Get urlFluxrss
     *
     * @return string
     */
    public function getUrlFluxrss()
    {
        return $this->urlFluxrss;
    }


    /**
     * Set iconeFluxrss
     *
     * @param string $iconeFluxrss
     *
     * @return Fluxrss
     */


     public function setIconeFLuxrss($iconeFluxrss)
     {
      //  if ($iconeFluxrss != null) {
      //    $strm = fopen($iconeFluxrss->getRealPath(), 'rb');
      //    $this->iconeFluxrss = stream_get_contents($strm);
      //  }

       $this->iconeFluxrss = $iconeFluxrss;
    return $this;
     }


    /**
     * Get iconeFLuxrss
     *
     * @return string
     */
    public function getIconeFluxrss()
    {
        if ($this->iconeFluxrss != null) {
            return base64_encode(stream_get_contents($this->iconeFluxrss));
        } else {
            return $this->iconeFluxrss;
        }
    }
}
