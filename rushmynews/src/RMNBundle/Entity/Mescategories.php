<?php

namespace RMNBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mescategories
 *
 * @ORM\Table(name="mescategories")
 * @ORM\Entity(repositoryClass="RMNBundle\Repository\MescategoriesRepository")
 */
class Mescategories
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="RMNBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false))
     */
    private $idProfilMescategories;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="RMNBundle\Entity\Categorie")
     * @ORM\JoinColumn(nullable=false))
     */
    private $idCategorieMescategories;

    /**
     * @var string
     *
     * @ORM\Column(name="couleur_mescategories", type="string", length=6)
     */
    private $couleurMescategories;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProfilMescategories
     *
     * @param integer $idProfilMescategories
     *
     * @return Mescategories
     */
    public function setIdProfilMescategories($idProfilMescategories)
    {
        $this->idProfilMescategories = $idProfilMescategories;

        return $this;
    }

    /**
     * Get idProfilMescategories
     *
     * @return int
     */
    public function getIdProfilMescategories()
    {
        return $this->idProfilMescategories;
    }

    /**
     * Set idCategorieMescategories
     *
     * @param integer $idCategorieMescategories
     *
     * @return Mescategories
     */
    public function setIdCategorieMescategories($idCategorieMescategories)
    {
        $this->idCategorieMescategories = $idCategorieMescategories;

        return $this;
    }

    /**
     * Get idCategorieMescategories
     *
     * @return int
     */
    public function getIdCategorieMescategories()
    {
        return $this->idCategorieMescategories;
    }

    /**
     * Set couleurMescategories
     *
     * @param string $couleurMescategories
     *
     * @return Mescategories
     */
    public function setCouleurMescategories($couleurMescategories)
    {
        $this->couleurMescategories = $couleurMescategories;

        return $this;
    }

    /**
     * Get couleurMescategories
     *
     * @return string
     */
    public function getCouleurMescategories()
    {
        return $this->couleurMescategories;
    }
}
