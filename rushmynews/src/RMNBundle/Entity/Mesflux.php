<?php

namespace RMNBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mesflux
 *
 * @ORM\Table(name="mesflux")
 * @ORM\Entity(repositoryClass="RMNBundle\Repository\MesfluxRepository")
 */
class Mesflux
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="RMNBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false))
     */
    private $idUserMesflux;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="RMNBundle\Entity\Fluxrss")
     * @ORM\JoinColumn(nullable=false))
     */
    private $idFluxrssMesflux;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="RMNBundle\Entity\Categorie")
     * @ORM\JoinColumn()
     */
    private $idCategorieMesflux;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_ajout_mesflux", type="datetime")
     */
    private $dateAjoutMesflux;

    /**
     * @var int
     *
     * @ORM\Column(name="refresh_mesflux", type="integer", nullable=true)
     */
    private $refreshMesflux;

    /**
     * @var int
     *
     * @ORM\Column(name="rotation_mesflux", type="integer", nullable=true)
     */
    private $rotationMesflux;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUserMesflux
     *
     * @param integer $idUserMesflux
     *
     * @return Mesflux
     */
    public function setIdUserMesflux($idUserMesflux)
    {
        $this->idUserMesflux = $idUserMesflux;

        return $this;
    }

    /**
     * Get idUserMesflux
     *
     * @return int
     */
    public function getIdUserMesflux()
    {
        return $this->idUserMesflux;
    }

    /**
     * Set idFluxrssMesflux
     *
     * @param integer $idFluxrssMesflux
     *
     * @return Mesflux
     */
    public function setIdFluxrssMesflux($idFluxrssMesflux)
    {
        $this->idFluxrssMesflux = $idFluxrssMesflux;

        return $this;
    }

    /**
     * Get idFluxrssMesflux
     *
     * @return int
     */
    public function getIdFluxrssMesflux()
    {
        return $this->idFluxrssMesflux;
    }

    /**
     * Set idCategorieMesflux
     *
     * @param integer $idCategorieMesflux
     *
     * @return Mesflux
     */
    public function setIdCategorieMesflux($idCategorieMesflux)
    {
        $this->idCategorieMesflux = $idCategorieMesflux;

        return $this;
    }

    /**
     * Get idCategorieMesflux
     *
     * @return int
     */
    public function getIdCategorieMesflux()
    {
        return $this->idCategorieMesflux;
    }

    /**
     * Set dateAjoutMesflux
     *
     * @param \DateTime $dateAjoutMesflux
     *
     * @return Mesflux
     */
    public function setDateAjoutMesflux($dateAjoutMesflux)
    {
        $this->dateAjoutMesflux = $dateAjoutMesflux;

        return $this;
    }

    /**
     * Get dateAjoutMesflux
     *
     * @return \DateTime
     */
    public function getDateAjoutMesflux()
    {
        return $this->dateAjoutMesflux;
    }

    /**
     * Set refreshMesflux
     *
     * @param integer $refreshMesflux
     *
     * @return Mesflux
     */
    public function setRefreshMesflux($refreshMesflux)
    {
        $this->refreshMesflux = $refreshMesflux;

        return $this;
    }

    /**
     * Get refreshMesflux
     *
     * @return int
     */
    public function getRefreshMesflux()
    {
        return $this->refreshMesflux;
    }

    /**
     * Set rotationMesflux
     *
     * @param integer $rotationMesflux
     *
     * @return Mesflux
     */
    public function setRotationMesflux($rotationMesflux)
    {
        $this->rotationMesflux = $rotationMesflux;

        return $this;
    }

    /**
     * Get rotationMesflux
     *
     * @return int
     */
    public function getRotationMesflux()
    {
        return $this->rotationMesflux;
    }
}
