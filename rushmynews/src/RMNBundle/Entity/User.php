<?php


namespace RMNBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Entity(repositoryClass="RMNBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 */

class User extends BaseUser
{
   /**
    * @ORM\Id
    * @ORM\Column(name="id", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_user", type="string", length=30, nullable=true)
     * @Assert\Length(max = 30)
     */
    private $nomUser;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom_user", type="string", length=30, nullable=true)
     * @Assert\Length(max = 30)
     */
    private $prenomUser;

    /**
     * @var string
     *
     * @ORM\Column(name="ville_user", type="string", length=40, nullable=true)
     * @Assert\Length(max = 40)
     */
    private $villeUser;

    /**
     * @var string
     *
     * @ORM\Column(name="pays_user", type="string", length=40, nullable=true)
     * @Assert\Length(max = 40)
     */
    private $paysUser;

    /**
     * @var string
     *
     * @ORM\Column(name="image_user", type="blob", nullable=true)
     */
    private $imageUser;

    /**
     * @var int
     *
     * @ORM\Column(name="refresh_user", type="integer", nullable=true)
     * @Assert\Range(max = 10)
     */
    private $refreshUser;

    /**
     * @var int
     *
     * @ORM\Column(name="rotation_user", type="integer", nullable=true)
     * @Assert\Range(max = 10)
     */
    private $rotationUser;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pseudonymeUser
     *
     * @param string $pseudonymeUser
     *
     * @return User
     */
    public function setPseudonymeUser($pseudonymeUser)
    {
        $this->pseudonymeUser = $pseudonymeUser;

        return $this;
    }

    /**
     * Get pseudonymeUser
     *
     * @return string
     */
    public function getPseudonymeUser()
    {
        return $this->pseudonymeUser;
    }

    /**
     * Set mailUser
     *
     * @param string $mailUser
     *
     * @return User
     */
    public function setMailUser($mailUser)
    {
        $this->mailUser = $mailUser;

        return $this;
    }

    /**
     * Get mailUser
     *
     * @return string
     */
    public function getMailUser()
    {
        return $this->mailUser;
    }

    /**
     * Set nomUser
     *
     * @param string $nomUser
     *
     * @return User
     */
    public function setNomUser($nomUser)
    {
        $this->nomUser = $nomUser;

        return $this;
    }

    /**
     * Get nomUser
     *
     * @return string
     */
    public function getNomUser()
    {
        return $this->nomUser;
    }

    /**
     * Set prenomUser
     *
     * @param string $prenomUser
     *
     * @return User
     */
    public function setPrenomUser($prenomUser)
    {
        $this->prenomUser = $prenomUser;

        return $this;
    }

    /**
     * Get prenomUser
     *
     * @return string
     */
    public function getPrenomUser()
    {
        return $this->prenomUser;
    }

    /**
     * Set villeUser
     *
     * @param string $villeUser
     *
     * @return User
     */
    public function setVilleUser($villeUser)
    {
        $this->villeUser = $villeUser;

        return $this;
    }

    /**
     * Get villeUser
     *
     * @return string
     */
    public function getVilleUser()
    {
        return $this->villeUser;
    }

    /**
     * Set paysUser
     *
     * @param string $paysUser
     *
     * @return User
     */
    public function setPaysUser($paysUser)
    {
        $this->paysUser = $paysUser;

        return $this;
    }

    /**
     * Get paysUser
     *
     * @return string
     */
    public function getPaysUser()
    {
        return $this->paysUser;
    }

    /**
     * Set imageUser
     *
     * @param string $imageUser
     *
     * @return User
     */


     public function setImageUser($imageUser)
     {
       if ($imageUser != null) {
         $strm = fopen($imageUser->getRealPath(), 'rb');
         $this->imageUser = stream_get_contents($strm);
       }
    return $this;
     }


    /**
     * Get imageUser
     *
     * @return string
     */
    public function getImageUser()
    {
        if ($this->imageUser != null) {
            return base64_encode(stream_get_contents($this->imageUser));
        } else {
            return $this->imageUser;
        }
    }

    /**
     * Set refreshUser
     *
     * @param integer $refreshUser
     *
     * @return User
     */
    public function setRefreshUser($refreshUser)
    {
        $this->refreshUser = $refreshUser;

        return $this;
    }

    /**
     * Get refreshUser
     *
     * @return int
     */
    public function getRefreshUser()
    {
        return $this->refreshUser;
    }

    /**
     * Set rotationUser
     *
     * @param integer $rotationUser
     *
     * @return User
     */
    public function setRotationUser($rotationUser)
    {
        $this->rotationUser = $rotationUser;

        return $this;
    }

    /**
     * Get rotationUser
     *
     * @return int
     */
    public function getRotationUser()
    {
        return $this->rotationUser;
    }
}
