<?php

namespace RMNBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class RMNBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
