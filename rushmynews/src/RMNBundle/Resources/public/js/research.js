google.load('feeds', '1');
var feedList = new Array();


	function resetfeeds() {
	  	$("#form_search").fadeOut('slow', function() {
			$('#recherche').val("").focus();
			$("#form_search").empty();
		});
	  return false;
	}

	function findfeeds() {
	  var q = $.trim($('#recherche').val());
	  if(q == "") {
		  resetfeeds();
      	  return false;
	  }


	  google.feeds.findFeeds(q, function(result) {
	  if (!result.error) {
		var html = '';
		for (var i = 0; i < result.entries.length; i++) {
		  var entry = result.entries[i];
		  feedList[i] = entry.url;
			html += '<div id="feed-' + i + '">';
			html += '<form action="./add" method="POST">';
			html += '	<h3><img src="//s2.googleusercontent.com/s2/favicons?domain=' + entry.link + '"" alt="favicon de '+ removeHTMLTags(entry.title) +'" /> ' + removeHTMLTags(entry.title) + '</h3>';
		  html += '	<p class="snippet">' + removeHTMLTags(entry.contentSnippet) + '</p>';
			html += '<input type="hidden" id="url_search" value="'+ entry.url +'" />';
		  html += '<button id="ajouter_flux_rechercher" type="submit" >Ajouter flux</a> ';
		  html += '</form>';
		  html += '</div>';




		}


		$("#form_search").fadeOut('slow', function() {
			$('html, body').animate({scrollTop:0}, 'slow');
			$("#form_search").empty();
			$("#form_search").append(html);
			$("#form_search").show();
		});
	   }
	});
	  return false;
	}


	function removeHTMLTags(html){
 		var cleanHTML =
			html.replace(/&(lt|gt);/g, function (strMatch, p1){
 		 		return (p1 == "lt")? "<" : ">";
				});
 		return cleanHTML.replace(/<\/?[^>]+(>|$)/g, "");
	}

    function getfeed(i) {
		var divcount = 'div#feedcontent-' + i + " > div";
		if($(divcount).length > 0) {
				$('div#feedcontent-' + i).animate({"height": "toggle"}, { duration: 500 });
				if($('div#feed-' + i).find("span.showhide").text() == "Preview Feed")
				   $('div#feed-' + i).find("span.showhide").text("Hide Feed Preview");
				else
				   $('div#feed-' + i).find("span.showhide").text("Preview Feed");
			return;
		}
		var feedURL = feedList[i];
		var feed = new google.feeds.Feed(feedURL);
		feed.setNumEntries(20)
		var html = '';

		feed.load(function(result) {
			if (!result.error) {
				for (var c = 0; c < result.feed.entries.length; c++) {
					var entry = result.feed.entries[c];
					var ct = c+1;
					html += '<div id="item-' + c + '"><h4>' + ct + '. <a target="_blank" href="' + entry.link + '">';
					html += entry.title + '</a> ' + entry.publishedDate.substring(5,16) + '</h4><!-- <small>' + entry.contentSnippet + '</small> --></div>';
				}
				$('div#feedcontent-' + i).addClass("feedItems").append(html);
				$('div#feed-' + i).find("span.showhide").text("Hide Feed Preview");
			}
		});
	}

  $(document).ready(function(){

    $('#recherche').keypress(function(e) {
        if(e.which == 13) {
            jQuery(this).blur();
            jQuery('#submit_search').focus().click();
            return false;
        }
    });

    $("#submit_search").click(findfeeds);
    $("h1").click(resetfeeds);

	$("span.showhide").on("click", function(event) {
		getfeed($(this).attr("rel"));
	});

    $('#recherche').focus();

});
