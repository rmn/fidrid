#+TITLE: Rush My News
#+EMAIL: http://rushmynews.com
#+OPTIONS: toc:nil author:nil num:nil
#+OPTIONS: reveal_title_slide:"<h1>%t</h1>%e"
#+REVEAL_THEME: sky
#+REVEAL_TRANS: linear
#+REVEAL_SPEED: fast

* Démarrage

   Intitulé de projet : « lecteur de flux »

** « lecteur de flux »

   Programme assez large :

   #+ATTR_REVEAL: :frag (appear)
    - Flux RSS/ATOM ?
    - tweets ?
    - mur Facebook ?
    - e-mails ?
    - autres... ?

** Nos choix

   #+ATTR_REVEAL: :frag (appear)
   - un cas pour commencer ( RSS/ATOM )
   - technologie : Drupal (7)
   - utiliser un outillage et des pratiques communes
   - dans un projet pédagogique !
     - chacun apporte son bagage
     - mise en pratique de ce qu'on a appris
     - partage d'expérience

* Organisation
** Mode d'organisation

    #+ATTR_REVEAL: :frag (appear)
   Notre souhait : explorer le besoin avant de se lancer dans les développements.

    #+ATTR_REVEAL: :frag (appear)
   Mode de travail hybride (en Y) :
    #+ATTR_REVEAL: :frag (appear)
   - une branche d'exploration technologiques
   - une branche d'exploration fonctionnelle
   - accord commun sur les fonctionnalités / technos

    #+ATTR_REVEAL: :frag (appear)
    Puis ensuite :
    #+ATTR_REVEAL: :frag (appear)
   - découpage des tâches
   - réalisation des développements

** Premières difficultés

   #+ATTR_REVEAL: :frag (appear)
   Lors de l'étape de réconciliation du besoin et des solutions techniques :

   #+ATTR_REVEAL: :frag (appear)
   - on a trouvé des modules RSS/ATOM disponibles avec Drupal
   - mais qui ne permettent pas de réaliser ce qu'on souhaite.

** Avis d'expert

   #+ATTR_REVEAL: :frag (appear)
   Nous avons sollicité notre expert technique en technologies back-end :

   #+ATTR_REVEAL: :frag (appear)
   merci Maxime !

   #+ATTR_REVEAL: :frag (appear)
   Suite à son avis, on oriente les développements sur symfony.

* User eXperience
** Parcours utilisateur

   [[./parcoursuser.jpeg]]

** Maquettes filaires

   [[./Mockups.pdf]]

** Maquette statique

   [[./maquette-html/homepage.html]]

* Choix Technologiques / Back End
** Les données

   [[./MCD.png]]

** Le cadre pour agiter les données

   [[./pile.svg]]

* Projet, bilans
** Outillage utilisé
*** git

    #+ATTR_REVEAL: :frag (appear)
    - apprentissage difficile, concepts parfois obscurs
    - trouver l'outillage pour son environnement
    - impose un processus de travail à l'équipe
      - branche par "sprint" ?
      - branche par développeur ?
    - l'occasion d'apprendre de nouvelles techniques (cherry-picking notamment)

*** gitlab (framagit)

    https://framagit.org/rmn/fidrid

** Perspectives

    #+ATTR_REVEAL: :frag (appear)
    - Attention, Laurent tu ne vas pas aimer :
    - un gros rework :-)
    - « interactiver » le biniou
      - état de lecture des news
      - rafraîchissement de la liste
      - utilisation de websockets
