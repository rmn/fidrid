<?php

namespace RMNBundle\Service;

use RMNBundle\Entity\Article;
use DOMDocument;
use DateTime;
use DateInterval;

class ArticlesPurge
{
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

     /**
     * Removes articles in database older than a number of days
     *
     * @param integer $daysAgo  Delete articles older than Nth days ago
     *
     * @return undefined
     */
    public function runPurge()
    {
        $articleRepo = $this->em->getRepository('RMNBundle:Article');

        $now      = new DateTime;
        $interval = new DateInterval('P'.Article::MAX_AGE.'D');
        // $interval = new DateInterval('PT'.$daysAgo.'H'); // En heures, pour les tests

        $query = $articleRepo->createQueryBuilder('a')
               ->where('a.date_article < :date')
               ->setParameter('date', $now->sub($interval))
               ->orderBy('a.date_article', 'ASC')
               ->getQuery();

        $articles = $query->getResult();

        foreach( $articles as $article ) {
            $this->em->remove($article);
            $this->em->flush();
        }
    }

    /**
     * Removes ALL articles in database
     *
     * @return undefined
     */
    public function runPurgeAll()
    {
        $articleRepo = $this->em->getRepository('RMNBundle:Article');
        $articles = $articleRepo->findAll();

        foreach( $articles as $article ) {
            $this->em->remove($article);
            $this->em->flush();
        }
    }
}
