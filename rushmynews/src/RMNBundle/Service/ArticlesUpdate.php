<?php

namespace RMNBundle\Service;

use RMNBundle\Entity\Article;
use DOMDocument;
use DateTime;
use DateInterval;
use Exception;

class ArticlesUpdate
{
    protected $em;

    protected $reader;

    public function __construct($em, $reader)
    {
        $this->em     = $em;
        $this->reader = $reader;
    }

    /**
     * Updates article tables from feeds list table.
     * This method is intended to be used by external scripts
     *
     * @param  none
     *
     * @return undefined
     */
    public function runUpdate()
    {
        $feedRep  = $this->em->getRepository('RMNBundle:Fluxrss');
        $artRep   = $this->em->getRepository('RMNBundle:Article');
        $feedList = $feedRep->findAll();

        foreach ($feedList as $feed) {
            try {
                $this->getRSSFeed($feed, Article::MAX_AGE);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    /**
     * Retrieves article liste from an RSS feed and inserts informations
     * in the articles table.
     *
     * @param  object      $feed  a \RMNBundle\Entity\fluxrss object
     *
     * @return undefined
     */
    private function getRSSFeed($feed, $daysAgo)
    {
        $now      = new DateTime;
        $interval = new DateInterval('P'.$daysAgo.'D');


        // Gets the feed from the URL and parses it.
        // Throw items older than Nth days ago.
        //
        $feedContent = $this->reader->getFeedContent($feed->getUrlFluxrss(),
                                                     $now->sub($interval));

        $items = $feedContent->getItems();

        foreach ( $items as $item ) {

            // Put informations in Doctrine
            //
            $this->saveArticle($item, $feed);
        }
    }

    /**
     * Sends gathered informations to Doctrine
     *
     * @param  array      $values an associative array 'XMLValueName' => value
     * @param  object     $feed   the current feed Object
     *
     * @return undefined
     */
    private function saveArticle($item, $feed)
    {
        // Vérification avant insertion sur le critère :
        //
        // unique( url_article + idFluxrss_article )
        //
        $articleRepo = $this->em->getRepository('RMNBundle:Article');

        if ( null !== $articleRepo->findOneBy(
            array('url_article'       => $item->getLink(),
                  'idFluxrss_article' => $feed))) {
            return;
        }

        // Insertion de l'article dans la table
        //
        $article = new Article();

        $article->setTitre(          $item->getTitle());
        $article->setDate(           $item->getUpdated());
        $article->setUrlArticle(     $item->getLink());
        $article->setContentArticle( $item->getDescription());

        $article->setIdFluxrss($feed);

        $this->em->persist($article);
        $this->em->flush();

        return;
    }
}
