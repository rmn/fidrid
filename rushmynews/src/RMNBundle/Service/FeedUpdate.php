<?php

namespace RMNBundle\Service;

use RMNBundle\Entity\Fluxrss;
use DateTime;
use DateInterval;
use Exception;

class FeedUpdate
{
    protected $em;

    protected $reader;

    public function __construct($em, $reader)
    {
        $this->em     = $em;
        $this->reader = $reader;
    }

    /**
     * Updates Fluxrss table with feed meta-informations
     * This method is intended to be used by an internal controller
     *
     * @param  none
     *
     * @return undefined
     */
    public function runUpdate($feedUrl)
    {
        // $feedRep  = $this->em->getRepository('RMNBundle:Fluxrss');
        // $feedList = $feedRep->find($feedUrl);

        try {
            return $this->getRSSFeed($feedUrl, '1');
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Retrieves meta informations from an RSS feed and inserts them in
     * the returned associative array:
     *  - string TITLE : feed's title
     *  - string ICON  : feed's website favicon (binary)
     *
     * @param  object      $feed  a \RMNBundle\Entity\fluxrss object
     *
     * @return array              an associative array with three elements
     */
    private function getRSSFeed($feedUrl)
    {
        $now      = new DateTime;
        $interval = new DateInterval('P1D');

        // Gets the feed from the URL and parses it.
        // Throw items older than Nth days ago.
        //
        $feedContent = $this->reader->getFeedContent($feedUrl,
                                                     $now->sub($interval));

        // Split URL and compose favicon's URL
        //
        $urlSplit = parse_url($feedUrl);

        $urlJoin  = (isset($urlSplit['scheme'])) ? $urlSplit['scheme'].'://' : 'http://';
        $urlJoin .= $urlSplit['host'];
        // $urlJoin .= (isset($urlSplit['port'])) ? ':'.$urlSplit['port'] : '' ;
        $urlJoin .= '/favicon.ico';

        // Return an array of the needed informations
        //
        $feedMeta = array();
        $feedMeta['title'] = $feedContent->getTitle();
        $feedMeta['icon']  = $urlJoin;
        $feedMeta['url']   = $feedUrl;

        return $feedMeta;
    }

    /**
     * Gets favicon
     *
     * @param  string     $url   the favicon URL
     *
     * @return string            the binary favicon stream
     */
    private function getIcon($url)
    {
        // Crée un fichier temporaire
        //
        $fileHandler = tmpfile();

        $curlHandler = curl_init($url);

        curl_setopt($curlHandler, CURLOPT_FILE,   $fileHandler);
        curl_setopt($curlHandler, CURLOPT_HEADER, 0);

        curl_exec($curlHandler);
        curl_close($curlHandler);

        // Récupération dans une variable de l'icône enregistré
        //
        fseek($fileHandler, 0);
        $icon = fread($fileHandler, 8192);

        // Efface le fichier temporaire
        //
        fclose($fileHandler);


        return $icon;
    }
}
