<?php

namespace RMNBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FeedControllerTest extends WebTestCase
{
    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/feed/list');
    }

    public function testSearch()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/feed/search');
    }

    public function testAdd()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/feed/add');
    }

}
